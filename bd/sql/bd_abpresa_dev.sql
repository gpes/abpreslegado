--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

-- Started on 2019-04-16 11:22:40 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12397)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2242 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 193 (class 1259 OID 16446)
-- Name: arquivos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arquivos (
    id integer NOT NULL,
    cod_arquivo character varying(255) NOT NULL,
    titulo_arquivo character varying(255) NOT NULL,
    descricao_arquivo character varying(255) NOT NULL,
    path_arquivo character varying(255) NOT NULL,
    tipo_arquivo_id integer NOT NULL
);


ALTER TABLE public.arquivos OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16444)
-- Name: arquivos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arquivos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arquivos_id_seq OWNER TO postgres;

--
-- TOC entry 2243 (class 0 OID 0)
-- Dependencies: 192
-- Name: arquivos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arquivos_id_seq OWNED BY public.arquivos.id;


--
-- TOC entry 183 (class 1259 OID 16395)
-- Name: categorias; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categorias (
    id integer NOT NULL,
    titulo_categoria character varying(255) NOT NULL
);


ALTER TABLE public.categorias OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 16393)
-- Name: categorias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categorias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorias_id_seq OWNER TO postgres;

--
-- TOC entry 2244 (class 0 OID 0)
-- Dependencies: 182
-- Name: categorias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categorias_id_seq OWNED BY public.categorias.id;


--
-- TOC entry 181 (class 1259 OID 16387)
-- Name: phinxlog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phinxlog (
    version bigint NOT NULL,
    migration_name character varying(100),
    start_time timestamp without time zone,
    end_time timestamp without time zone,
    breakpoint boolean DEFAULT false NOT NULL
);


ALTER TABLE public.phinxlog OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16430)
-- Name: praticas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.praticas (
    id integer NOT NULL,
    titulo_pratica character varying(255) NOT NULL,
    descricao_pratica character varying(255) NOT NULL,
    categorias_id integer NOT NULL
);


ALTER TABLE public.praticas OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16462)
-- Name: praticas_arquivos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.praticas_arquivos (
    id integer NOT NULL,
    praticas_id integer NOT NULL,
    arquivos_id integer NOT NULL
);


ALTER TABLE public.praticas_arquivos OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 16460)
-- Name: praticas_arquivos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.praticas_arquivos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.praticas_arquivos_id_seq OWNER TO postgres;

--
-- TOC entry 2245 (class 0 OID 0)
-- Dependencies: 194
-- Name: praticas_arquivos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.praticas_arquivos_id_seq OWNED BY public.praticas_arquivos.id;


--
-- TOC entry 190 (class 1259 OID 16428)
-- Name: praticas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.praticas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.praticas_id_seq OWNER TO postgres;

--
-- TOC entry 2246 (class 0 OID 0)
-- Dependencies: 190
-- Name: praticas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.praticas_id_seq OWNED BY public.praticas.id;


--
-- TOC entry 197 (class 1259 OID 16480)
-- Name: praticas_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.praticas_tags (
    id integer NOT NULL,
    praticas_id integer NOT NULL,
    tags_id integer NOT NULL
);


ALTER TABLE public.praticas_tags OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 16478)
-- Name: praticas_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.praticas_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.praticas_tags_id_seq OWNER TO postgres;

--
-- TOC entry 2247 (class 0 OID 0)
-- Dependencies: 196
-- Name: praticas_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.praticas_tags_id_seq OWNED BY public.praticas_tags.id;


--
-- TOC entry 185 (class 1259 OID 16403)
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    descricao_tag character varying(255) NOT NULL
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 16401)
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO postgres;

--
-- TOC entry 2248 (class 0 OID 0)
-- Dependencies: 184
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- TOC entry 187 (class 1259 OID 16411)
-- Name: tipo_arquivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_arquivo (
    id integer NOT NULL,
    descricao_tipo_arquivo character varying(255) NOT NULL
);


ALTER TABLE public.tipo_arquivo OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16409)
-- Name: tipo_arquivo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_arquivo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_arquivo_id_seq OWNER TO postgres;

--
-- TOC entry 2249 (class 0 OID 0)
-- Dependencies: 186
-- Name: tipo_arquivo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_arquivo_id_seq OWNED BY public.tipo_arquivo.id;


--
-- TOC entry 189 (class 1259 OID 16419)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    nome character varying(255) NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    tipo_usuario character varying(255) NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 16417)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;

--
-- TOC entry 2250 (class 0 OID 0)
-- Dependencies: 188
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- TOC entry 2076 (class 2604 OID 16449)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arquivos ALTER COLUMN id SET DEFAULT nextval('public.arquivos_id_seq'::regclass);


--
-- TOC entry 2071 (class 2604 OID 16398)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categorias ALTER COLUMN id SET DEFAULT nextval('public.categorias_id_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 16433)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas ALTER COLUMN id SET DEFAULT nextval('public.praticas_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 16465)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_arquivos ALTER COLUMN id SET DEFAULT nextval('public.praticas_arquivos_id_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 16483)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_tags ALTER COLUMN id SET DEFAULT nextval('public.praticas_tags_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 16406)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- TOC entry 2073 (class 2604 OID 16414)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_arquivo ALTER COLUMN id SET DEFAULT nextval('public.tipo_arquivo_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 16422)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- TOC entry 2229 (class 0 OID 16446)
-- Dependencies: 193
-- Data for Name: arquivos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arquivos (id, cod_arquivo, titulo_arquivo, descricao_arquivo, path_arquivo, tipo_arquivo_id) FROM stdin;
\.


--
-- TOC entry 2251 (class 0 OID 0)
-- Dependencies: 192
-- Name: arquivos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arquivos_id_seq', 1, false);


--
-- TOC entry 2219 (class 0 OID 16395)
-- Dependencies: 183
-- Data for Name: categorias; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categorias (id, titulo_categoria) FROM stdin;
1	RUP
2	XP
3	SCRUM
4	RUP
5	XP
6	SCRUM
\.


--
-- TOC entry 2252 (class 0 OID 0)
-- Dependencies: 182
-- Name: categorias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categorias_id_seq', 6, true);


--
-- TOC entry 2217 (class 0 OID 16387)
-- Dependencies: 181
-- Data for Name: phinxlog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.phinxlog (version, migration_name, start_time, end_time, breakpoint) FROM stdin;
20170814214129	Migration	2017-12-07 17:54:57	2017-12-07 17:54:57	f
\.


--
-- TOC entry 2227 (class 0 OID 16430)
-- Dependencies: 191
-- Data for Name: praticas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.praticas (id, titulo_pratica, descricao_pratica, categorias_id) FROM stdin;
1	Planning Pocker	Descrição da boa prática Planning Pocker	3
2	Reunios Diarias	Descrição da boa prática Reunios Diarias	2
3	Desenvolvimento em Pares	Descrição da boa prática Desenvolvimento em Pares	2
4	Disponibilidade do Cliente	Descrição da boa prática Disponibilidade do Cliente	2
5	Small Releases	Descrição da boa prática Small Releases	2
6	Testes de Aceitação	Descrição da boa prática Testes de Aceitação	2
7	Planning Pocker	Descrição da boa prática Planning Pocker	3
8	Reunios Diarias	Descrição da boa prática Reunios Diarias	2
9	Desenvolvimento em Pares	Descrição da boa prática Desenvolvimento em Pares	2
10	Disponibilidade do Cliente	Descrição da boa prática Disponibilidade do Cliente	2
11	Small Releases	Descrição da boa prática Small Releases	2
12	Testes de Aceitação	Descrição da boa prática Testes de Aceitação	2
\.


--
-- TOC entry 2231 (class 0 OID 16462)
-- Dependencies: 195
-- Data for Name: praticas_arquivos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.praticas_arquivos (id, praticas_id, arquivos_id) FROM stdin;
\.


--
-- TOC entry 2253 (class 0 OID 0)
-- Dependencies: 194
-- Name: praticas_arquivos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.praticas_arquivos_id_seq', 1, false);


--
-- TOC entry 2254 (class 0 OID 0)
-- Dependencies: 190
-- Name: praticas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.praticas_id_seq', 12, true);


--
-- TOC entry 2233 (class 0 OID 16480)
-- Dependencies: 197
-- Data for Name: praticas_tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.praticas_tags (id, praticas_id, tags_id) FROM stdin;
1	1	1
2	1	3
3	2	3
4	2	4
5	3	4
6	3	2
7	4	3
8	4	4
9	5	4
10	6	4
11	6	5
12	1	1
13	1	3
14	2	3
15	2	4
16	3	4
17	3	2
18	4	3
19	4	4
20	5	4
21	6	4
22	6	5
\.


--
-- TOC entry 2255 (class 0 OID 0)
-- Dependencies: 196
-- Name: praticas_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.praticas_tags_id_seq', 22, true);


--
-- TOC entry 2221 (class 0 OID 16403)
-- Dependencies: 185
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tags (id, descricao_tag) FROM stdin;
1	GERENCIA
2	DESENVOLVIMENTO
3	REUNIOES
4	XP
5	TESTES
6	GERENCIA
7	DESENVOLVIMENTO
8	REUNIOES
9	XP
10	TESTES
\.


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 184
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tags_id_seq', 10, true);


--
-- TOC entry 2223 (class 0 OID 16411)
-- Dependencies: 187
-- Data for Name: tipo_arquivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_arquivo (id, descricao_tipo_arquivo) FROM stdin;
\.


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 186
-- Name: tipo_arquivo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_arquivo_id_seq', 1, false);


--
-- TOC entry 2225 (class 0 OID 16419)
-- Dependencies: 189
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (id, nome, username, password, email, tipo_usuario) FROM stdin;
1	Jonathas Almeida	jonathas.almeida	12345	jonathasarts@gmail.com	ADM
2	Alber Jonathas	alber.jonathas	12345	alber.jonathas@gmail.com	NORMAL
3	Samyra Almeida	samyra.almeida	12345	samyra.almeida@gmail.com	NORMAL
4	Heremita Brasileiro	heremita.brasileiro	12345	heremita.brasileiro@gmail.com	ADM
5	Jonathas Almeida	jonathas.almeida	12345	jonathasarts@gmail.com	ADM
6	Alber Jonathas	alber.jonathas	12345	alber.jonathas@gmail.com	NORMAL
7	Samyra Almeida	samyra.almeida	12345	samyra.almeida@gmail.com	NORMAL
8	Heremita Brasileiro	heremita.brasileiro	12345	heremita.brasileiro@gmail.com	ADM
\.


--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 188
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 8, true);


--
-- TOC entry 2092 (class 2606 OID 16454)
-- Name: arquivos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arquivos
    ADD CONSTRAINT arquivos_pkey PRIMARY KEY (id);


--
-- TOC entry 2082 (class 2606 OID 16400)
-- Name: categorias_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categorias
    ADD CONSTRAINT categorias_pkey PRIMARY KEY (id);


--
-- TOC entry 2080 (class 2606 OID 16392)
-- Name: phinxlog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phinxlog
    ADD CONSTRAINT phinxlog_pkey PRIMARY KEY (version);


--
-- TOC entry 2094 (class 2606 OID 16467)
-- Name: praticas_arquivos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_arquivos
    ADD CONSTRAINT praticas_arquivos_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 16438)
-- Name: praticas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas
    ADD CONSTRAINT praticas_pkey PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 16485)
-- Name: praticas_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_tags
    ADD CONSTRAINT praticas_tags_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 16408)
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 2086 (class 2606 OID 16416)
-- Name: tipo_arquivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_arquivo
    ADD CONSTRAINT tipo_arquivo_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 16427)
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 16455)
-- Name: arquivos_tipo_arquivo_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arquivos
    ADD CONSTRAINT arquivos_tipo_arquivo_id FOREIGN KEY (tipo_arquivo_id) REFERENCES public.tipo_arquivo(id);


--
-- TOC entry 2100 (class 2606 OID 16473)
-- Name: praticas_arquivos_arquivos_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_arquivos
    ADD CONSTRAINT praticas_arquivos_arquivos_id FOREIGN KEY (arquivos_id) REFERENCES public.arquivos(id) ON DELETE SET NULL;


--
-- TOC entry 2099 (class 2606 OID 16468)
-- Name: praticas_arquivos_praticas_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_arquivos
    ADD CONSTRAINT praticas_arquivos_praticas_id FOREIGN KEY (praticas_id) REFERENCES public.praticas(id) ON DELETE SET NULL;


--
-- TOC entry 2097 (class 2606 OID 16439)
-- Name: praticas_categorias_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas
    ADD CONSTRAINT praticas_categorias_id FOREIGN KEY (categorias_id) REFERENCES public.categorias(id) ON DELETE SET NULL;


--
-- TOC entry 2101 (class 2606 OID 16486)
-- Name: praticas_tags_praticas_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_tags
    ADD CONSTRAINT praticas_tags_praticas_id FOREIGN KEY (praticas_id) REFERENCES public.praticas(id);


--
-- TOC entry 2102 (class 2606 OID 16491)
-- Name: praticas_tags_tags_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.praticas_tags
    ADD CONSTRAINT praticas_tags_tags_id FOREIGN KEY (tags_id) REFERENCES public.tags(id);


--
-- TOC entry 2241 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-04-16 11:22:40 -03

--
-- PostgreSQL database dump complete
--

