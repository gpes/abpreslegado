import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BoaPraticaComponent } from "./components/praticas/boa-pratica/boa-pratica.component";

import { TelaInicialComponent } from "./components/tela-inicial/tela-inicial.component";
import { CadastroComponent } from "./components/usuario/cadastro/cadastro.component";
import { LoginComponent } from './components/usuario/login/login.component';
import { CreateComponent } from './components/categorias/create/create.component';

const routes: Routes = [
  {
    path: "boa-pratica",
    component: BoaPraticaComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "",
    component: TelaInicialComponent
  },
  {
    path: "cadastrar",
    component: CadastroComponent
  },
  {
    path: "categorias/cadastrar",
    component: CreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
