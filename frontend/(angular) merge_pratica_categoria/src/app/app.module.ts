import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BoaPraticaComponent } from "./components/praticas/boa-pratica/boa-pratica.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { TelaInicialComponent } from "./components/tela-inicial/tela-inicial.component";
import { CadastroComponent } from './components/usuario/cadastro/cadastro.component';
import { LoginComponent } from './components/usuario/login/login.component';
import { CreateComponent } from './components/categorias/create/create.component';

@NgModule({
  declarations: [
    AppComponent,
    BoaPraticaComponent,
    NavbarComponent,
    FooterComponent,
    TelaInicialComponent,
    CadastroComponent,
    LoginComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule, 
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
