import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Categoria } from './Categoria';
import { Observable, of } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { Pratica } from './Pratica';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: localStorage.getItem('SECRET_KEY')
  })
};

@Injectable({
  providedIn: 'root'
})
export class PraticasService {
  readonly URL_API = 'https://abtest22.herokuapp.com/api';

  constructor(private http: HttpClient) { }

  /* getPraticas(): Observable<any> {
    return this.http.get(this.URL_API + '/praticas');
  } */
  getPraticas(): Observable<Pratica[]> {
    return this.http.get<Pratica[]>(this.URL_API + '/praticas').pipe(
      tap(pratica => console.log('fetched pratica')),
      catchError(this.handleError('getPraticas', []))
    );
  }

  getPratica(id: number): Observable<Pratica> {
    const url = `${this.URL_API}/${id}`;
    return this.http.get<Pratica>(url).pipe(
      tap(_ => console.log(`fetched pratica id =${id}`)),
      catchError(this.handleError<Pratica>(`getPratica id=${id}`))
    );
  }

  addPratica(pratica: Pratica, catId: number): Observable<Pratica> {
    return this.http
      .post<Pratica>(`${this.URL_API}/praticas/${catId}`, pratica, httpOptions)
      .pipe(
        tap((pratica: Pratica) =>
          console.log(`added pratica w/ id=${pratica.id}`)
        ),
        catchError(this.handleError<Pratica>('addPratica'))
      );
  }

  /* editPratica(pratica: Pratica): Observable<any> {
    return this.http.put(`${this.URL_API}/praticas/${pratica.id}`, pratica, httpOptions);
  } */

  editPratica(pratica): Observable<any> {
    console.log(pratica.categorias)
    return this.http
      .put(`${this.URL_API}/praticas/${pratica.id}`, pratica, httpOptions)
      .pipe(
        tap(_ => console.log(`edite pratica id=${pratica.id}`)),
        catchError(this.handleError<any>('editePratica'))
      );
  }
  /* 
  deletePratica(id: number): Observable<Pratica> {
    return this.http.delete<Pratica>(`${this.URL_API}/praticas/${id}`);
  }  */

  deletePratica(id): Observable<Pratica> {
    return this.http
      .delete<Pratica>(`${this.URL_API}/praticas/${id}`, httpOptions)
      .pipe(
        tap(_ => console.log(`delete pratica id=${id}`)),
        catchError(this.handleError<Pratica>('deletePratica'))
      );
  }


  getCategorias(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(this.URL_API + '/categorias').pipe(
      tap(cat => console.log('fetched categoria')),
      catchError(this.handleError('getCategorias', []))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
