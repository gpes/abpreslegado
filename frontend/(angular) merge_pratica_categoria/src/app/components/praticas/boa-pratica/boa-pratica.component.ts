import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { Pratica } from '../Pratica';
import { Categoria } from '../../categorias/entity/categoria';
import { PraticasService } from '../praticas.service';

@Component({
  selector: 'app-boa-pratica',
  templateUrl: './boa-pratica.component.html',
  styleUrls: ['./boa-pratica.component.css']
})
export class BoaPraticaComponent implements OnInit {
  praticaForm: FormGroup;
  categorias: Categoria[];
  praticas: Pratica[];
  submitted = false;
  success = false;
  successMsg = '';
  edit = false;
  idcategoria = null;
  logado = true;
  delete_pratica: Pratica;
  editPratica: { id: number, titulo: String, categorias: Categoria[] };
  //addPratica: Pratica;

  constructor(
    private router: Router,
    private praticasService: PraticasService,
    private formBuilder: FormBuilder
  ) {
    this.praticaForm = this.formBuilder.group({
      titulo: ['', Validators.required],
      categoria: ['', Validators.required]
    });
  }

  //praticas: Pratica[] = [{id: 1, titulo: 'Teste1', categoria: 'Gerência'}, {id: 2, titulo: 'Teste2', categoria: 'Gerência'}];

  ngOnInit() {
    if (localStorage.getItem('SECRET_KEY') === null) {
      this.logado = false;
    }
    this.getCategorias();
    this.getPraticas();
    this.praticaForm = this.formBuilder.group({
      titulo: ['', Validators.required],
      categoria: ['', Validators.required]
    });
  }

  onSubmit() {
    event.preventDefault();
    this.addPratica(this.praticaForm.value);
    console.log(this.praticaForm.value);
  }

  /*  onSubmit(p: NgForm) {
    this.submitted = true;
    if (this.praticaForm.invalid) {
      return;
    }
    console.log(this.id);
    this.praticas.push({
      id: ++this.id[0].id, titulo: p.value.titulo, categorias: p.value.categoria
    })
    this.success = true;
    this.successMsg = 'Cadastro realizado com sucesso!';
    setTimeout(() => this.success = false, 1300);
    //this.praticaForm.reset();

  } */
  /* onSubmit(p: NgForm) {
    this.submitted = true;
    if (this.praticaForm.invalid) {
      return;
    }
    this.addPratica = {id: 0, titulo: p.value.titulo, categorias: p.value.categoria}
    this.praticas.push(this.addPratica);
    this.success = true;
    this.successMsg = 'Cadastro realizado com sucesso!';
    this.router.navigate(['add-pratica']);
    this.praticaService.addPratica(this.addPratica);
    setTimeout(() => this.success = false, 1300);

  }
 */

  addPratica(pratica: Pratica): void {
    this.praticasService
      .addPratica(pratica, this.praticaForm.value.categoria)
      .subscribe(data => this.ngOnInit());

  }

  onEdite(pratica: Pratica) {
    window.localStorage.removeItem('editpraticaId');
    let praticaID = window.localStorage.setItem(
      'editpraticaId',
      pratica.id.toString()
    );
    this.editPratica = pratica;
    this.edit = !this.edit;
  }

  async edite(pratica: String, categoria: String) {

    let praticaId = window.localStorage.getItem('editpraticaId');
    if (!praticaId) {
      this.router.navigate(['/boa-pratica']);
      return;
    }
    this.editPratica = {
      id: this.editPratica.id,
      titulo: pratica,
      categorias: [{
        id: this.editPratica.categorias[0].id,
        titulo: this.editPratica.categorias[0].titulo
      }]
    };
    this.praticas = this.praticas.filter(
      prat => prat.id !== this.editPratica.id
    );
    await this.praticasService.editPratica(this.editPratica).subscribe();
    this.router.navigateByUrl('/boa-pratica');
    //this.router.navigate(['/boa-pratica']);
  }

  onDelete(pratica: Pratica) {
    window.localStorage.removeItem('removepratica');
    let praticaId = window.localStorage.setItem(
      'deletepraticaId',
      pratica.id.toString()
    );
    this.delete_pratica = pratica;
    //this.delete(this.delete_pratica);
  }

  async delete() {
    let praticaId = window.localStorage.getItem('deletepraticaId');
    let pratica = this.praticas.filter(
      prat => prat.id.toString() === praticaId
    );
    await this.praticasService
      .deletePratica(praticaId)
      .subscribe(data => this.praticas);
    this.success = true;
    this.successMsg = `Boa prática ${pratica[0].titulo} removida!`;
    setTimeout(() => (this.success = false), 1300);
    this.router.navigate(['/boa-pratica']);
    this.ngOnInit();
  }

  async getCategorias() {
    return await this.praticasService.getCategorias().subscribe(
      cat => {
        this.categorias = cat;
      },
      err => {
        console.log(err);
      }
    );
  }

  async getPraticas() {
    return await this.praticasService.getPraticas().subscribe(
      prat => {
        this.praticas = prat;
      },
      err => {
        console.log(err);
      }
    );
  }
}
