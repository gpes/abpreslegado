import { Categoria } from '../categorias/entity/categoria';

export class Pratica {
  id: number;
  titulo: string;
  categorias: Categoria[];
}
