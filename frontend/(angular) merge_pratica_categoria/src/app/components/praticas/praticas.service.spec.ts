import { TestBed } from '@angular/core/testing';

import { PraticasService } from './praticas.service';

describe('PraticasService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PraticasService = TestBed.get(PraticasService);
    expect(service).toBeTruthy();
  });
});
