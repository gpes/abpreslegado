import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpResponse, HttpHeaders, HttpClient } from '@angular/common/http';
import { Categoria } from '../entity/categoria';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  private currentUser: Object;
  
  // url = 'http://abpres-backend.herokuapp.com/api/categorias';
  url = 'http://abtest22.herokuapp.com/api/categorias';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('SECRET_KEY')
    }),
  }



  constructor(private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
  }

  create = (categoria): Observable<HttpResponse<Categoria>> => {
    return this.http.post<Categoria>(
      this.url,
      categoria,
      { observe: 'response' })
  }

}
