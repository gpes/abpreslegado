import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { CategoriaService } from '../service/categoria.service';
import {Categoria} from '../entity/categoria'

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  categoria: Categoria
  createForm: FormGroup

  constructor(private categoriaService: CategoriaService) {
    this.categoria = new Categoria()
    this.createForm = new FormGroup({
      titulo: new FormControl(this.categoria.titulo, [
        Validators.required,
      ])
    })
  }

  ngOnInit() {

  }

  onSubmit() {
    const data = this.createForm.value
    Object.keys(data).forEach( key => {
      this.categoria[key] = data[key]
    });
    this.categoriaService.create(this.categoria).subscribe(
      res => console.log(res),
      err => console.log(err)
    )
  }
}
