import { Component, OnInit } from '@angular/core';
import { FormsModule, EmailValidator, NgForm } from '@angular/forms';
import { UsuarioService } from '../service/usuario.service';
import { Usuario } from '../entity/usuario';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  errors: Object
  returnUrl: string;

  constructor(private usuarioService: UsuarioService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  getInstanceErrors = () => {
    if (this.errors == null)
      this.errors = Object()
    return this.errors
  }

  onSubmit(form: NgForm) {
    if (form.value.nome == undefined || form.value.nome == '')
      this.getInstanceErrors()["nome"] = "Preencha este campo!"
    if (form.value.email == undefined || form.value.email == '')
      this.getInstanceErrors()["email"] = "Preencha este campo!"
    else if (form.value.email.indexOf('@') == -1)
      this.getInstanceErrors()["email"] = "Informe um e-mail válido."
    if (form.value.senha == undefined || form.value.senha == '')
      this.getInstanceErrors()["senha"] = "Preencha este campo!"
    else if (form.value.senha.split('').length < 6)
      this.getInstanceErrors()["senha"] = "A senha deve ter no mínimo 6 caracteres."
    if (form.value.confirmaSenha == undefined || form.value.confirmaSenha == '')
      this.getInstanceErrors()["confirmaSenha"] = "Preencha este campo!"
    else if (form.value.senha !== form.value.confirmaSenha)
      this.getInstanceErrors()["confirmaSenha"] = "Senhas não coincidem, tente novamente."
    if (this.errors == null) {
      let usuario = new Usuario(form.value.nome, form.value.email, form.value.senha, 'BASICO')
      this.usuarioService.createUsuario(usuario).subscribe(
        res => {
          console.log(res)
          this.router.navigate(['/login'])
        },
        err => {
          console.log(err.status)
          this.router.navigate([this.returnUrl])
        }
      )
    }
  }
}