export class Usuario {

    id: number
    username: String
    email: String
    senha: String
    tipo: String

    constructor(username: String, email: String, senha: String, tipo: String) {
        this.username = username
        this.email = email
        this.senha = senha
        this.tipo = tipo
    }

    setId(id: number) {
        this.id = id
    }

}
