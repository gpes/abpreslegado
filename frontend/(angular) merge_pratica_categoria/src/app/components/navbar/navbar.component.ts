import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../usuario/service/usuario.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  currentUser = JSON.parse(localStorage.getItem('currentUser'))

  constructor(private usuarioService: UsuarioService,
    private router: Router,
    private route: ActivatedRoute) {
    }


  ngOnInit() {
  }

  logout() {
    this.usuarioService.logout()
    window.location.reload();
  }

}
