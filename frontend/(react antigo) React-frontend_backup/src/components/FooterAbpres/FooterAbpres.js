import React from 'react';
import {Layout} from 'antd';

const {Footer: FooterAbpres} = Layout;

export const Footer = (props) => {
    return (
        <FooterAbpres style={{textAlign: 'center'}}>
            Ant Design ©2018 Created by Ant UED
        </FooterAbpres>
    );
}