import React from 'react';
import {Layout, Menu} from 'antd';

const {Header} = Layout;

export const HeaderAbpres = (props) => {
    return (
        <Header className="backgroundColor headerAbpres">
            <Menu
                mode="horizontal"
                className="headerAbpresMenu"
            >
                <Menu.Item key="home">Home</Menu.Item>
                <Menu.Item key="frameworks">Frameworks</Menu.Item>
                <Menu.Item key="oquesaoboaspraticas">O que são boas práticas?</Menu.Item>
                <Menu.Item key="projeteagil">Projete Ágil</Menu.Item>
                <Menu.Item key="login" className='headerAbpresLogin'>Login</Menu.Item>
            </Menu>
        </Header>
    );
}