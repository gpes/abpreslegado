import React, {Component} from 'react';
import {HeaderAbpres, Footer} from '../../components';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Routes from "../Routes";
import {Layout} from 'antd';

const {Content} = Layout;

export default class LayoutAbpres extends Component {
    render() {
        return (
            <Layout className="layout">
                <HeaderAbpres/>
                <Content style={{padding: '0 10%', minHeight: 280}}>
                    <Router>
                        <Switch>
                            <Route path='/' component={Routes}/>
                        </Switch>
                    </Router>
                </Content>
                <Footer/>
            </Layout>
        )
    }
}