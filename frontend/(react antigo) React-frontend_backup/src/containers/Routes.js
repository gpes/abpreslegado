import React, {Component, Fragment} from 'react';
import { Route, Redirect } from 'react-router-dom';
import Home from './Home/Home'
import Login from './Auth/Login';
import Register from './Auth/Register';
import Category from "./Category/Category";

export default class Routes extends Component{
    render() {
        return (
            <Fragment>
                <Route exact path="/" render={() => (
                    <Redirect to="/home"/>
                )}/>
                <Route path='/home' component={Home}/>
                <Route path='/login' component={Login}/>
                <Route path='/register' component={Register}/>
                <Route path='/category' component={Category}/>
            </Fragment>
        )
    }
}