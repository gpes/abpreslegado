## ABPRES

Neste repositório você vai encontrar a aplicação front-end do ABPRES feita em [React](https://reactjs.org/), onde aqui você irá encontrar:
* O projeto atualizado pronto para receber suas gambiarras haha;
* Configurar ambiente a aplicação;
* Rodar a aplicação em modo desenvolvimento;
* Rodar os testes testes;
* Fazer o build da aplicação.

### Requisitos para suir
* [NPM](https://www.npmjs.com/get-npm) instalado na versão LTS (10.xx.xx);
* Editor de texto/IDE de sua preferência ([WebStorm](https://www.jetbrains.com/webstorm/download/#section=windows), [Visual Studio Code](https://code.visualstudio.com/download), [Sublime](https://www.sublimetext.com/3) e etc).

### Como configuro o projeto?
Após clonar o projeto em sua máquina, navegue até a raiz do projeto e execute o seguinte comando no terminal, certifique-se que o NPM está instalado:

### `npm install`

Aguarde que se você não tem uma conexão boa talvez demore um pouco, este comando vai baixar todas as dependências do projeto.<br>
Agora é necessário que você indique qual IP e porta a aplicação [back-end](https://gitlab.com/gpes/abpres) está sendo executada e alterar no arquivo de referência a API:

* Abra o arquivo **api.js** que se encontra em **/src/services**;
* Na linha **baseURL: "localhost:7000"** altere para o IP e porta da aplicação back-end que está sendo utilizada.
 
### Como eu rodo a aplicação em modo desenvolvimento?
Vá até a raiz do projeto e execute o seguinte comando:
### `npm start`

Abra o navegador no seguinte endereço [http://localhost:3000](http://localhost:3000) para visualizar a aplicação.<br>
A página automaticamente vai recarregar quando você salvar mudanças.<br>

### Como eu rodo os testes da aplicação?
Na raiz do projeto execute o comando:
### `npm test`

### Como eu faço build da aplicação??
Na raiz do projeto execute o comando:
### `npm run build`

Boa sorte!