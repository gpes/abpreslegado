import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Usuario } from '../entity/usuario';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private currentUserSubject: BehaviorSubject<Usuario>;
  public currentUser: Observable<Usuario>;
  // url = 'http://abpres-backend.herokuapp.com/api/usuarios';
  url = 'http://abtest22.herokuapp.com/api/usuarios';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
  }

  constructor(private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute) {
    this.currentUserSubject = new BehaviorSubject<Usuario>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  getUsuarios(): Observable<Usuario> {
    return this.http.get<Usuario>(this.url)
  }

  createUsuario(usuario): Observable<HttpResponse<Usuario>> {
    return this.http.post<Usuario>(
      this.url,
      usuario,
      { observe: 'response' })
  }

  public get currentUserValue(): Usuario {
    return this.currentUserSubject.value;
  }

  login(login: string, password: string) {
    this.http.post<Usuario>(
      `${this.url}/autenticar`,
      {
        'login': login,
        'password': password
      },
      {
        observe: 'response'
      }
    ).subscribe(user => {
      // login successful if there's a jwt token in the response
      if (user && user.headers.get('Authorization')) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user.body));
        localStorage.setItem('SECRET_KEY', user.headers.get('Authorization'));
        this.currentUserSubject.next(user.body);
        localStorage.setItem('reloadNavbar', 'true');
        this.router.navigate([this.route.snapshot.queryParams['returnUrl'] || '/'])
      }
      return user;
    });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('SECRET_KEY');
    this.currentUserSubject.next(null);
  }
}
