import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoaPraticaComponent } from './boa-pratica.component';

describe('BoaPraticaComponent', () => {
  let component: BoaPraticaComponent;
  let fixture: ComponentFixture<BoaPraticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoaPraticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoaPraticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
