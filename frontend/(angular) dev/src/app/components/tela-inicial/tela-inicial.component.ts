import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tela-inicial',
  templateUrl: './tela-inicial.component.html',
  styleUrls: ['./tela-inicial.component.css']
})
export class TelaInicialComponent implements OnInit {

  constructor() {

    if (localStorage.getItem('reloadNavbar') != null
      && localStorage.getItem('reloadNavbar') === 'true') {
      window.location.reload()
      localStorage.setItem('reloadNavbar', 'false');
    }
  }

  ngOnInit() {
  }

}
