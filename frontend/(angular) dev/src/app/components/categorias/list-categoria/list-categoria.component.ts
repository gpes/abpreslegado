import { Component, OnInit } from '@angular/core';
import { Categoria } from '../entity/categoria';
import { CategoriaService } from '../service/categoria.service';
import { faCoffee, faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-list-categoria',
  templateUrl: './list-categoria.component.html',
  styleUrls: ['./list-categoria.component.css']
})
export class ListCategoriaComponent implements OnInit {

  constructor(
    private categoriaService: CategoriaService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  categorias: Array<Categoria> = null
  faPen = faPen;
  faTrash = faTrash
  ngOnInit() {
    this.categoriaService.get().subscribe(
      res => this.categorias = res
    )
  }

}
