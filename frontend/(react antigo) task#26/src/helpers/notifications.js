import { notification } from 'antd';

export const alertError = (title, description) => (
    notification['error']({
        message: title,
        description,
    })
);

export const alertSuccess = (title, description) => (
    notification['success']({
        message: title,
        description,
    })
);
