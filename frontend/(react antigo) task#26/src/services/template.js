import api from './api'

export const requisicaoExemplo = async () => {
    try{
        return await api.get('/endpoint');
    }catch (e) {
        throw e
    }
}
