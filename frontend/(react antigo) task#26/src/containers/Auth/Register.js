import React, {Component, Fragment} from 'react';
import {Form, Input, Tooltip, Icon, Row, Col, Button } from 'antd';


class Register extends Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
        console.log("Submetido");
    }

    handleReset = () => {
        this.props.form.resetFields();
      }

    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('As senhas digitadas não coincidem!');
        } else {
            callback();
        }
    }

    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], {force: true});
        }
        callback();
    }

    render() {
        const {getFieldDecorator} = this.props.form;


        return (
            <Fragment>
                <Row>
                    <Col span={8} offset={5}>
                        <h3 className="titleRegister">Dados de Usuário</h3>
                    </Col>
                </Row>
                <Form className="w50 mauto" onSubmit={this.handleSubmit} layout="vertical">
                    <Form.Item className="inputSizeNickname"
                        label="Nome de usuário"
                    >
                    {getFieldDecorator('nickname', {
                        rules: [{required: true, message: 'Preencha este campo!', whitespace: true}],
                    })(
                        <Input placeholder="Seu nome para a comunidade GPES" />
                    )}
                    </Form.Item>
                    <Form.Item
                        className="inputSize"
                        label="E-mail"
                    >
                        {getFieldDecorator('email', {
                            rules: [{
                                type: 'email', message: 'Coloque um e-mail válido!',
                            }, {
                                required: true, message: 'Preencha este campo!',
                            }],
                        })(
                            <Input placeholder="E-mail para contato"/>
                        )}
                    </Form.Item>
                    <Form.Item
                        className="inputSize" 
                        label={(
                            <span>Senha&nbsp;
                                <Tooltip title="Tamanho mínimo de 6 dígitos">
                                <Icon type="question-circle-o"/>
                                </Tooltip>
                            </span>
                        )}
                    >
                        {getFieldDecorator('password', {
                            rules: [{
                                required: true, message: 'Preencha este campo!',
                            }, {
                                validator: this.validateToNextPassword,
                            }],
                        })(
                            <Input type="password" placeholder="Senha"/>
                        )}
                    </Form.Item>
                    <Form.Item
                        className="inputSize" 
                        label="Confirme senha"
                    >
                        {getFieldDecorator('confirm', {
                            rules: [{
                                required: true, message: 'Confirme a senha!',
                            }, {
                                validator: this.compareToFirstPassword,
                            }],
                        })(
                            <Input type="password" onBlur={this.handleConfirmBlur} placeholder="Repita a senha"/>
                        )}
                    </Form.Item>
                    <Row>
                        <Col span={3} offset={0}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" /* onClick={this.handleSubmit }*/ >Salvar</Button>
                            </Form.Item>
                        </Col>
                        <Col span={3} offset={0}>
                            <Form.Item>
                                <Button  onClick={this.handleReset}>
                                Cancelar
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Form>
            </Fragment>
        );
    }
}

export default Form.create()(Register);