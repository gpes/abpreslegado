import React, {Component} from 'react';
import {Form, Icon, Input, Button} from 'antd';

class Login extends Component {
    _handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    }

    render() {
        const {getFieldDecorator} = this.props.form;
        return (
            <Form onSubmit={this._handleSubmit} className="login-form">
                <Form.Item>
                    {getFieldDecorator('userName', {
                        rules: [{
                            type: 'email', message: 'Utiliza um e-mail válido!',
                        }, {
                            required: true, message: 'Preencha este campo!',
                        }],
                    })(
                        <Input prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>} placeholder="E-mail"/>
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{required: true, message: 'Preencha este campo!'}],
                    })(
                        <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password" placeholder="Senha"/>
                    )}
                </Form.Item>
                <Form.Item>
                    <a className="login-form-forgot" href="">Esqueci minha senha</a>
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        Entrar
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create()(Login);