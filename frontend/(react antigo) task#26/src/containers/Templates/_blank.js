//Este arquivo é um exemplo modelo que deve ser copiado para outra página.
//Contém a estrutura base contendo inclusive requisição assincrona.
//Caso use a função assincrona renomei-a com um nome adequado. Caso contrário a exclua.
//
//Não modifique este arquivo
import React, {Component} from 'react';
import {ContentContainer} from '../../components'

import {alertError} from '../../helpers'
import {requisicaoExemplo} from '../../services'

export default class TemplateBlank extends Component{
    state = {
        dados: null
    };

    componentDidMount(){
        this.asyncRequisition()
    }

    asyncRequisition = async () => {
        try{
            const dados = await requisicaoExemplo();
            this.setState({dados})

        }catch (e) {
            alertError('Titulo da Notificação de Erro', 'Descrição da Notificação de Erro')
            alertError('Titulo da Notificação de Erro', e.message)
        }
    };

    render() {
        return (
            <ContentContainer>
                <h1 className='text-center'>Título do template</h1>
            </ContentContainer>
        )
    }
}
