import React, {Component} from 'react';
import {HeaderAbpres} from '../../components';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Routes from "../Routes";
import {Layout} from 'antd';

const {Content} = Layout;

export default class LayoutAbpres extends Component {
    render() {
        return (
            <Router>
                <Layout className="layout">
                    <HeaderAbpres/>
                    <Content style={{padding: '0 10%', minHeight: 280}} className="mt3 container md-bg-white">
                        <Switch>
                            <Route path='/' component={Routes}/>
                        </Switch>
                    </Content>
                </Layout>
            </Router>
        )
    }
}
