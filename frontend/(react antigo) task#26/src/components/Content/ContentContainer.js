import React from 'react';

export const ContentContainer = ({children}) => {
    return (
        <div className='mt2'>
            {children}
        </div>
    );
}
