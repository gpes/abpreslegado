import React from 'react';
import {Link} from 'react-router-dom'
import {Layout, Menu, Dropdown, Button} from 'antd';
import Brand from '../../assets/img/technology.svg'

const {Header} = Layout;
const {Item} = Menu;

const menuUser = (
    <Menu>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Editar</Link>
        </Menu.Item>
        <Menu.Divider></Menu.Divider>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Minhas submissões</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Meus debates</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Práticas favoritas</Link>
        </Menu.Item>
        <Menu.Divider></Menu.Divider>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Logout</Link>
        </Menu.Item>
    </Menu>
);

const menuAssuntos = (
    <Menu>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Gerência</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">PDS</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Codificação</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Arquitetura</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Teste</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Scrum</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">Ágil</Link>
        </Menu.Item>
        <Menu.Item>
            <Link rel="noopener noreferrer" to="/">BDD</Link>
        </Menu.Item>
    </Menu>
);

export const HeaderAbpres = (props) => {
    return (
        <Layout className="layout">
            <Header className="md-color-white bottomOutboxShadow green-ifpb headerAbpres">
                <img src={Brand} className="logo" />
                <Menu mode="horizontal" className='menu' style={{ lineHeight: '64px' }}>
                    <Item><Link to='/gpes'>ABPRES</Link></Item>
                    <Item><Link to='/gpes'>Práticas</Link></Item>
                    <Item>
                        <Dropdown overlay={menuAssuntos} placement="bottomLeft">
                            <Button className="headerAbpresButton">Assuntos</Button>
                        </Dropdown>
                    </Item>
                    <Item className='pull-right'>
                        <Dropdown overlay={menuUser} placement="bottomLeft">
                            <Button className="headerAbpresButton">Assuntos</Button>
                        </Dropdown>
                    </Item>
                    </Menu>
            </Header>
        </Layout>
    );
}
