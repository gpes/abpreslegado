import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { BoaPraticaComponent } from "./components/praticas/boa-pratica/boa-pratica.component";
import { LoginComponent } from "./components/login/login.component";
import { TelaInicialComponent } from "./components/tela-inicial/tela-inicial.component";

const routes: Routes = [
  {
    path: "boa-pratica",
    component: BoaPraticaComponent
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "",
    component: TelaInicialComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
