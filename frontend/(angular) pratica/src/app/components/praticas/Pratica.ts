import { Categoria } from './Categoria';

export class Pratica {
  id: number;
  titulo: string;
  categorias: Categoria[];
}
