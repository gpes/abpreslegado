import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';
import { Router } from '@angular/router';

import { Pratica } from '../Pratica';
import { Categoria } from '../Categoria';
import { PraticasService } from '../praticas.service';

@Component({
  selector: 'app-boa-pratica',
  templateUrl: './boa-pratica.component.html',
  styleUrls: ['./boa-pratica.component.css']
})
export class BoaPraticaComponent implements OnInit {

  praticaForm: FormGroup;
  categorias: Categoria[];
  praticas: Pratica[] = [];
  submitted = false;
  success = false;
  successMsg = '';
  edit = false;
  
  delete_pratica: Pratica;
  editPratica: Pratica;
  //addPratica: Pratica;

  constructor(private router: Router, private praticasService: PraticasService, private formBuilder: FormBuilder) {
    this.praticaForm = this.formBuilder.group({
      titulo: ['', Validators.required],
      categoria: ['', Validators.required]
    })
   }

  //praticas: Pratica[] = [{id: 1, titulo: 'Teste1', categoria: 'Gerência'}, {id: 2, titulo: 'Teste2', categoria: 'Gerência'}];

  ngOnInit() {
    /* if(!window.localStorage.getItem('token')) {
      this.router.navigate(['login']);
      return;
    } */
    this.getCategorias();
    this.getPraticas();
    this.praticaForm = this.formBuilder.group({
      titulo: ['', Validators.required],
      categoria: ['', Validators.required]
    })
  }

  onSubmit() {
    this.praticasService.addPratica(this.praticaForm.value)
      .subscribe(data => this.router.navigate(['boa-pratica']));
  }

 /*  onSubmit(p: NgForm) {
    this.submitted = true;
    if (this.praticaForm.invalid) {
      return;
    }
    console.log(this.id);
    this.praticas.push({
      id: ++this.id[0].id, titulo: p.value.titulo, categorias: p.value.categoria
    })
    this.success = true;
    this.successMsg = 'Cadastro realizado com sucesso!';
    setTimeout(() => this.success = false, 1300);
    //this.praticaForm.reset();

  } */
  /* onSubmit(p: NgForm) {
    this.submitted = true;
    if (this.praticaForm.invalid) {
      return;
    }
    this.addPratica = {id: 0, titulo: p.value.titulo, categorias: p.value.categoria}
    this.praticas.push(this.addPratica);
    this.success = true;
    this.successMsg = 'Cadastro realizado com sucesso!';
    this.router.navigate(['add-pratica']);
    this.praticaService.addPratica(this.addPratica);
    setTimeout(() => this.success = false, 1300);

  }
 */

  addPratica(): void {
    this.router.navigate(['add-pratica']);
  }

  onEdite(pratica: Pratica) {
    this.editPratica = pratica;
    window.localStorage.removeItem("editpraticaId");
    let praticaID = window.localStorage.setItem("editpraticaId", pratica.id.toString());
    console.log(praticaID)
    this.edit = !this.edit;
  }

  edite() {
    // falta conferir isso aqui
    let praticaId = window.localStorage.getItem("editPraticaId");
    console.log(praticaId)
    if (!praticaId) {
      this.router.navigate(['boa-pratica']);
      return;
    }
    this.editPratica = {id: this.editPratica.id, titulo: event.target['titulo-edit'].value, categorias: [Categoria[event.target['categoria'].value]]}
    this.praticas = this.praticas.filter(prat => prat.id !== this.editPratica.id);
    this.praticas.push(this.editPratica);
    this.praticasService.editPratica(this.editPratica).subscribe();
  }

  onDelete(pratica: Pratica) {
    window.localStorage.removeItem("removepratica");
    let praticaId = window.localStorage.setItem("deletepraticaId", pratica.id.toString());
    this.delete_pratica = pratica;
    //this.delete(this.delete_pratica);
  }
  
  delete() {
    let praticaId = window.localStorage.getItem("deletepraticaId");
    let pratica = this.praticas.filter(prat => prat.id.toString() === praticaId);
    this.praticas = this.praticas.filter(prat => prat !== pratica[0]);
    this.praticasService.deletePratica(praticaId).subscribe(data => this.praticas);
    this.success = true;
    this.successMsg = `Boa prática ${pratica[0].titulo} removida!`;
    setTimeout(() => this.success = false, 1300);
  }

  getCategorias() {
    return this.praticasService.getCategorias()
      .subscribe(cat => {
        this.categorias = cat
      }, err => {
        console.log(err);
      });
  }

  getPraticas() {
    return this.praticasService.getPraticas()
      .subscribe(prat => {
        this.praticas = prat;
        //console.log(this.praticas);
      }, err => {
        console.log(err);
      });
  }

}


