import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BoaPraticaComponent } from "./components/praticas/boa-pratica/boa-pratica.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { LoginComponent } from "./components/login/login.component";
import { TelaInicialComponent } from "./components/tela-inicial/tela-inicial.component";

@NgModule({
  declarations: [
    AppComponent,
    BoaPraticaComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    TelaInicialComponent
  ],
  imports: [
    BrowserModule, 
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
