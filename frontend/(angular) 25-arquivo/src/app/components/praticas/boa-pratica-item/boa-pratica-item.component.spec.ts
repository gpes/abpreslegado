import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoaPraticaItemComponent } from './boa-pratica-item.component';

describe('BoaPraticaItemComponent', () => {
  let component: BoaPraticaItemComponent;
  let fixture: ComponentFixture<BoaPraticaItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoaPraticaItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoaPraticaItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
