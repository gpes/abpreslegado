import { Component, OnInit, Input } from '@angular/core';

import { Pratica } from '../Pratica';

@Component({
  selector: 'app-boa-pratica-item',
  templateUrl: './boa-pratica-item.component.html',
  styleUrls: ['./boa-pratica-item.component.css']
})
export class BoaPraticaItemComponent implements OnInit {

  @Input() pratica: Pratica;

  constructor() { }

  ngOnInit() {
  }

}
