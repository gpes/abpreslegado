import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BoaPraticaComponent } from "./components/praticas/boa-pratica/boa-pratica.component";
import { BoaPraticaItemComponent } from "./components/praticas/boa-pratica-item/boa-pratica-item.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { LoginComponent } from "./components/login/login.component";
import { TelaInicialComponent } from "./components/tela-inicial/tela-inicial.component";
import { ArquivoComponent } from './components/arquivo/arquivo.component';

@NgModule({
  declarations: [
    AppComponent,
    BoaPraticaComponent,
    BoaPraticaItemComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    TelaInicialComponent,
    ArquivoComponent
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
