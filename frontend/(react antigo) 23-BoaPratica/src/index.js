import React from 'react';
import ReactDOM from 'react-dom';
import MainApp from './MainApp';
import '../node_modules/antd/dist/antd.css';
import './assets/css/index';

ReactDOM.render(<MainApp />, document.getElementById('root'));