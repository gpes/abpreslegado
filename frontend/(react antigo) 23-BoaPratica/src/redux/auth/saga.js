import {all, takeEvery, put, fork, call} from 'redux-saga/effects';
import {push} from 'react-router-redux';
import {getToken, clearToken} from '../../helpers/utility';
import actions from './actions';
import {doLogin} from '../../services'
import notification from "../../components/notification";
import sha1 from 'js-sha1'

const fakeApiCall = true; // auth0 or express JWT

export function* loginRequest() {
    yield takeEvery('LOGIN_REQUEST', function* ({identifier, password}) {
        try {
            password = sha1(password);
            const loginRequest = yield call(doLogin, {identifier, password});
            yield put({
                type: actions.LOGIN_SUCCESS,
                token: loginRequest.data.data.token,
                profile: loginRequest.data.data.user
            });
            yield put(push('/dashboard'));

        } catch (e) {
            notification("error", e.message);
            clearToken();
            yield put({type: actions.LOGIN_ERROR});
        }
    });
}

export function* loginSuccess() {
    yield takeEvery(actions.LOGIN_SUCCESS, function* (payload) {
        yield localStorage.setItem('id_token', payload.token);
    });
}

export function* loginError() {
    yield takeEvery(actions.LOGIN_ERROR, function* () {
    });
}

export function* logout() {
    yield takeEvery(actions.LOGOUT, function* () {
        clearToken();
    });
}

export function* checkAuthorization() {
    yield takeEvery(actions.CHECK_AUTHORIZATION, function* () {
        const token = getToken().get('idToken');
        if (token) {
            yield put({
                type: actions.LOGIN_SUCCESS,
                token,
                profile: 'Profile'
            });
        }
    });
}

export default function* rootSaga() {
    yield all([
        fork(checkAuthorization),
        fork(loginRequest),
        fork(loginSuccess),
        fork(loginError),
        fork(logout)
    ]);
}
