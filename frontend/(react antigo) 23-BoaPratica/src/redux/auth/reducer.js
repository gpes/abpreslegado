import actions from "./actions";

const initState = {idToken: null, loadingLogin: false};

export default function authReducer(state = initState, action) {
    switch (action.type) {
        case actions.LOGIN_REQUEST:
            return {...state, loadingLogin: true}
        case actions.LOGIN_ERROR:
            return {...state, loadingLogin: false}
        case actions.LOGIN_SUCCESS:
            return {
                idToken: action.token,
                loadingLogin: false
            };
        case actions.LOGOUT:
            return initState;
        default:
            return state;
    }
}
