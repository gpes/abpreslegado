import React, { Component, Fragment } from 'react'
import {Form, Select, Input, Button, Row, Col, Alert, Modal} from 'antd'

const { Option } = Select
const confirm = Modal.confirm
let id = 0
let idOption = 0
const opcoes = ["Gerência", "PDS", "Codificação", "Arquitetura", "Teste", "Scrum", "Ágil", "BDD"]
class Boapratica extends Component {

  state = {
    titulo: '',
    categoria: '',
    confirmaEnvio: '',
    praticas: [],
  }

  showConfirmModal(titulo) {
    return new Promise(resolve => {
      confirm({
      title: `Deseja realmente excluir ${titulo}?`,
      okText: 'Sim',
      okType: 'danger',
      cancelText: 'Não',
      onOk() {
        resolve(true);
      }
    })
    })
  }

  handleInputChange = e => {
    this.setState({titulo: e.target.value})
  }

  handleSelectChange = e => {
    this.setState({categoria: e})
  }

  onDelete = (id) => {
    this.state.praticas.filter(pratica => {
      if (pratica.id === id) {
        this.showConfirmModal(pratica.titulo).then(result => {
          if (result) {
            this.setState({
              praticas: [...this.state.praticas.filter(pratica => pratica.id !== id)],
              confirmaEnvio: <Alert className="align_center" message={`Boa prática ${pratica.titulo} removida!`} type="error" />
            })
            setTimeout(() => {this.setState({confirmaEnvio: ''})}, 1000)
          }      
        })
      }
    })
  }
  
  handleSubmit = e => {
    e.preventDefault()
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState(prevState =>{
          
          return {
            confirmaEnvio: <Alert className="align_center" message="Cadastro realizado com sucesso!" type="success" />,
            praticas: prevState.praticas.concat({
              id: ++id,
              titulo: values.titulo,
              categoria: values.select,
              editar: false
            })
          }
        })
        setTimeout(() => {this.setState({confirmaEnvio: ''})}, 1000)
      }
    }) 
  }

  salvarNovaPratica = i => {
    const novaPratica = {
      id: ++id,
      titulo: this.state.titulo,
      categoria: this.state.categoria,
      editar: false
    }
    const praticas = this.state.praticas.filter(pratica => pratica.id !== i) 
    this.setState({
      praticas: praticas.concat(novaPratica),
      confirmaEnvio: <Alert className="align_center" message="Alteração realizada com sucesso!" type="success" />,
    })
    setTimeout(() => {this.setState({confirmaEnvio: ''})}, 1000)
  }

  editar = id => {
    const praticas = this.state.praticas.map(pratica => {
      if (pratica.id === id) {
        pratica.editar = true
      }
      return pratica
    })
    this.setState({praticas})
  }

  render() {
    const { getFieldDecorator } = this.props.form

    let options = opcoes.map(op => {
      return (<Option key={++idOption} value={op}>{op}</Option>)
    })

    let conteudo = '';
    if (this.state.praticas.length === 0) {
      conteudo = (
      <tr className="table_tr hover">
        <td className="align_center">Nenhuma prática cadastrada!</td>
      </tr>
      )
    } else {
      conteudo = this.state.praticas.map( pratica => {
        if (pratica.editar) {
          return (
            <tr className="table_tr hover" key={pratica.id}>
              <td><Input type="text" 
                className="table--editar w100 align_center" 
                name={pratica.id} 
                placeholder={pratica.titulo}
                onChange={this.handleInputChange}
              /></td>
              <td>
                <Select placeholder="Escolha a categoria"  className="select-boapratica" onChange={this.handleSelectChange}>
                  {options}
                </Select>
              </td>
              <td className="w10 align_center a"><Button onClick={() => this.salvarNovaPratica(pratica.id)}><i className="fa fa-floppy-o" aria-hidden="true"></i></Button></td>
              <td className="align_center">
                <Button type="danger" onClick={() => this.onDelete(pratica.id)}>
                <i className="fa fa-trash" aria-hidden="true"></i>
                </Button>
              </td>
            </tr>  
          )
        } else {
          return (<tr className="table__tr hover" key={pratica.id}>
          <td className="align_center">{pratica.titulo}</td>
          <td className="align_center">{pratica.categoria}</td>
          <td className="align_center">
            <Button onClick={() => this.editar(pratica.id)}>
              <i className="fa fa-pencil" aria-hidden="true"></i>
            </Button>
          </td>
          <td className="align_center">
            <Button type="danger" onClick={() => this.onDelete(pratica.id)}>
              <i className="fa fa-trash" aria-hidden="true"></i>
            </Button>
          </td>
        </tr>)
        }
      })
    }

    return (
      <Fragment>
        <Row>
          <Col span={8} offset={5}>
            <h2 className="titleBoaPratica">Boa Prática</h2>
          </Col>
          <Col span={14} offset={5}>
            <Form layout="inline" onSubmit={this.handleSubmit}>
              <Form.Item label="Titulo">
                {getFieldDecorator('titulo', {
                    rules: [{ required: true, message: 'Preencha este campo!' }],
                })(
                    <Input onChange={this.handleInputChange} name="titulo" id="titulo" />
                )}
              </Form.Item>
              <Form.Item
                label="Categoria"
                >
                {getFieldDecorator('select', {
                    rules: [{ required: true, message: 'Preencha este campo!' },],
                })(         
                <Select placeholder="Escolha a categoria"  className="select-boapratica" onChange={this.handleSelectChange}>
                  {options}
                </Select>
                )}
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Salvar
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col span={14} offset={5}>
            {this.state.confirmaEnvio}    
          </Col>
        </Row>
        <Row>
        <Col span={14} offset={5}>
          <table className="w95">
            <thead>
              <tr className="table__tr table__header">
                <th className="align_center">
                  Titulo
                </th>
                <th className="align_center" >
                  Categoria
                </th>
                <th className="align_center" >
                  Editar
                </th>
                <th className="align_center">
                  Apagar
                </th>
              </tr>
            </thead>
            <tbody>
              {conteudo}
            </tbody>
          </table>
          </Col>
          </Row>
      </Fragment>
    )
  }
}

export default Form.create()(Boapratica);