import React, { Component, Fragment } from 'react';
import { Input, Button, Row, Col, Alert, Modal } from 'antd';

export default class Category extends Component{
  
  state = { 
    titulo: '',
    statusMessage: '',
    Categorias: [
      {id:1, title: 'Arquitetura', editar: false},
      {id:2, title: 'Implementação', editar: false},
      {id:3, title: 'Análise', editar: false},
      {id:4, title: 'Testes', editar: false},
      {id:5, title: 'BDD', editar: false},
      {id:6, title: 'TDD', editar: false},
      {id:7, title: 'Especificação', editar: false},
      {id:8, title: 'Gerência', editar: false},
      {id:9, title: 'Scrum', editar: false}]
    }

  _showMessageBox = (message, type,) => {
    this.setState({statusMessage: (<Row>
      <Col span={13} offset={6}>
      <br />
      <Alert message={message} type={type} className="align_center"/>
      </Col>
      </Row>)})
      setTimeout(() => {
        this.setState({statusMessage: ''})
      }, 1300);
  }

  _salvarCategoria = (e) => {
    if(this.state.titulo === ''){
      this._showMessageBox("Preencha este campo!", "error")
    }else{
      const existe = this.state.Categorias.some(x => x.title === this.state.titulo)
      if(existe){
        this._showMessageBox("Categoria já existe!", "error")  
      }else{
        let message = <div>Categoria {this.state.titulo} cadastrada!</div>
        this._showMessageBox(message, "success") 
        const novoId = this.state.Categorias.length + 1
        const novoTitulo = {id: novoId, title: this.state.titulo, editar: false}
        const Categorias = [...this.state.Categorias, novoTitulo]
        this.setState({
          Categorias, titulo: ''
        })
        document.getElementById('titulo').value = ''
    }
    }
  }

  _handleChange = (e) => {
    this.setState({titulo: e.target.value})
  }

  _salvarNovaCategoriaKey = (e) => {
    if(e.keyCode == 13){
      this._salvarNovaCategoria(e)
    }
  }

  _salvarCategoriaKey = (e) => {
    if(e.keyCode == 13){
      let btn = document.querySelector('#mybutton')
      btn.click()
    }
  }

  _salvarNovaCategoria = (e) => {
    let titulo = document.querySelector(`input[name="${e.target.id}"]`)
    const Categorias = this.state.Categorias.map(x => {
      if(x.id == e.target.id){
        if(titulo.value){
          let result = this.state.Categorias.some(x => {
            if(x.title == titulo.value){
              return true
            }else{
              return false
            }
          })
          if(result){
            this._showMessageBox("Categoria já existe!", "error") 
          }else{
            x.title = titulo.value
            let message = <div>Categoria {titulo.value} atualizada!</div>
            this._showMessageBox(message, "success")  
          }
        }
        else{
          this._showMessageBox("Preencha o campo!", "error")  
        } 
        x.editar = false
      }
      return x
     })

     this.setState({
       Categorias
     })
  }

  _editar = (e) => {
    const Categorias = this.state.Categorias.map(x => {
     if(x.id == e.target.id){
      x.editar = true
     }
    return x
    })
    this.setState({
      Categorias
    })
  }

  _showModal(titulo) {
    const confirm = Modal.confirm;
    return new Promise(resolve => {
      confirm({
        title: `Deseja realmente excluir ${titulo}?`,
        content: '',
        okText: 'Sim',
        okType: 'danger',
        cancelText: 'Não',
        onOk() {
          resolve(true)
        }
      });
    })
  }
  
  _apagar = (e) =>{
    let idApagar = e.target.id
    let titulo = this.state.Categorias.filter(x => {
      if(x.id == idApagar){
        return true
      }
      return false
    })
    this._showModal(titulo[0].title).then(result => {

      if(result){
        let message = <div>Categoria {titulo[0].title} removida!</div>
        this._showMessageBox(message, "error")  
  
        const Categorias = this.state.Categorias.filter(x => x.id != idApagar)
        this.setState({
          Categorias
        })
      }
    })
    
  }

  render() {
    let data = '';
    if(this.state.Categorias.length === 0 ){
      data = (<tr key="x.id" className="table__tr hover">
      <td className="w80 align_center" id="semcategoria">Nenhuma Categoria cadastrada!</td>
    </tr>)
    }else{
      data = this.state.Categorias.map(x => {
        if(x.editar){
          return (
            <tr key={x.id} className="table__tr hover">
              <input type="text" className="table--editar w100 align_center"  id={x.id} name={x.id} placeholder={x.title} onKeyDown={this._salvarNovaCategoriaKey}></input>
              <td className="w10 align_center a" alignItem="end"><Button id={x.id} onClick={this._salvarNovaCategoria}><i className="fa fa-floppy-o" aria-hidden="true"></i></Button></td>
              <td className="w10 align_center" id={x.id}><Button id={x.id} type="danger" onClick={this._apagar}><i className="fa fa-trash" aria-hidden="true"></i></Button></td>
            </tr>
          )
        }
        else{
          return (
            <tr key={x.id} className="table__tr hover">
            <td className="w80 align_center" id={x.id}>{x.title}</td>
            <td className="w10 align_center" id={x.id}><Button id={x.id} onClick={this._editar}><i className="fa fa-pencil" aria-hidden="true"></i></Button></td>
            <td className="w10 align_center" id={x.id}><Button id={x.id} type="danger" onClick={this._apagar}><i className="fa fa-trash" aria-hidden="true"></i></Button></td>
          </tr>
          )
        }
      })
    }

    return (
      <Fragment>
        <Row>
          <Col span={12} offset={6}><h1>Categoria</h1></Col>
          <Col span={12} offset={6}><Input size="large" placeholder="Digite uma Categoria" id="titulo" onChange={this._handleChange}  onKeyDown={this._salvarCategoriaKey}/></Col>
          <Col span={6}><Button size="large" onClick={this._salvarCategoria} id="mybutton">Salvar</Button></Col>
        </Row>
        <div className={this.state.statusMessage ? 'statusMessage' : 'statusDefault'}>
        {this.state.statusMessage}
        </div>
        {/* <br/>
        <br/> */}

        <Row>
          <Col span={14} offset={6}>
          <table className="w95">
            <thead>
              <tr className="table__tr table__header">
                <th className="align_center">
                  Nome
                </th>
                <th className="align_center" >
                  Editar
                </th>
                <th className="align_center">
                  Apagar
                </th>
              </tr>
            </thead>
            <tbody>
              {data}
            </tbody>
          </table>
          </Col>
        </Row>
      </Fragment>
    )
  }
}