import React, {Component, Fragment} from 'react';
import { Route, Redirect } from 'react-router-dom';
import Home from './Home/Home'
import Login from './Auth/Login';
import Register from './Auth/Register';
import Category from "./Category/Category";
import Quemsomos from "./Quemsomos/Quemsomos";
import Sobreoprojeto from "./Sobreoprojeto/Sobreoprojeto";
import Contato from "./Contato/Contato";
import Arquivo from "./Arquivo/Arquivo";
import Boapratica from "./Boapratica/Boapratica";

export default class Routes extends Component{
    render() {
        return (
            <Fragment>
                <Route exact path="/" render={() => (
                    <Redirect to="/gpes"/>
                )}/>
                <Route path='/gpes' component={Home}/>
                <Route path='/login' component={Login}/>
                <Route path='/cadastro' component={Register}/>
                <Route path='/categoria' component={Category}/>
                <Route path='/quemsomos' component={Quemsomos}/>
                <Route path='/contato' component={Contato}/>
                <Route path='/sobreoprojeto' component={Sobreoprojeto}/>
                <Route path='/arquivo' component={Arquivo}/>
                <Route path='/boapratica' component={Boapratica}/>
            </Fragment>
        )
    }
}