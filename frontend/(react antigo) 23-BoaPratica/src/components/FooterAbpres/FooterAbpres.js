import React from 'react';
import {Layout, Row, Col, Button} from 'antd';

const {Footer: FooterAbpres} = Layout;

export const Footer = (props) => {
    return (
        <FooterAbpres className="footerAbpres backgroundColor">
            <Row>
                <Col span={5}>
                    <p>Logo</p>
                </Col>
                <Col span={14}>
                    {/*<Col span={3}>*/}
                        <Button as="a" href="/quemsomos" className="footerAbpresButton">Quem somos</Button>
                    {/*</Col>*/}
                    {/*<Col span={3}>*/}
                        <Button as="a" href="/contato" className="footerAbpresButton">Contato</Button>
                    {/*</Col>*/}
                    {/*<Col span={3}>*/}
                        <Button as="a" href="http://www.ifpb.edu.br" className="footerAbpresButton">IFPB</Button>
                    {/*</Col>*/}
                    {/*<Col span={3}>*/}
                        <Button as="a" href="/sobreoprojeto" className="footerAbpresButton">Sobre o projeto</Button>
                    {/*</Col>*/}
                </Col>
                <Col span={5}>
                    <p align="right">© 2019 GPES - Grupo em Engenharia de Software</p>
                </Col>
            </Row>
        </FooterAbpres>
    );
}