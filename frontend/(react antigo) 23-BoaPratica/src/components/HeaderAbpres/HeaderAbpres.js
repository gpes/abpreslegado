import React from 'react';
import {Layout, Menu, Row, Col, Dropdown, Button} from 'antd';

const {Header} = Layout;

const menu = (
    <Menu>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Editar</a>
        </Menu.Item>
        <Menu.Divider></Menu.Divider>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Minhas submissões</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Meus debates</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Práticas favoritas</a>
        </Menu.Item>
        <Menu.Divider></Menu.Divider>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Logout</a>
        </Menu.Item>
    </Menu>
);

const menuAssuntos = (
    <Menu>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Gerência</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">PDS</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Codificação</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Arquitetura</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Teste</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Scrum</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">Ágil</a>
        </Menu.Item>
        <Menu.Item>
            <a rel="noopener noreferrer" href="/">BDD</a>
        </Menu.Item>
    </Menu>
);

export const HeaderAbpres = (props) => {
    return (
        <Header className="backgroundColor headerAbpres">
            <Row>
                <Col span={8}>
                    <Button as="a" href="/gpes" className="headerAbpresButton headerAbpresLeftButtons">ABPRES</Button>
                    <Button as="a" href="/gpes" className="headerAbpresButton headerAbpresLeftButtons">Práticas</Button>
                    <Dropdown overlay={menuAssuntos} placement="bottomLeft">
                        <Button className="headerAbpresButton">Assuntos</Button>
                    </Dropdown>
                </Col>
                <Col span={2} offset={14}>
                    <Button as="a" href="/cadastro" className="headerAbpresButton">Cadastro</Button>
                    <Button as="a" href="/login" className="headerAbpresButton headerAbpresLoginButton">Login</Button>
                    {/*<Dropdown overlay={menu} placement="bottomRight">*/}
                        {/*<Button size="large" className="headerAbpresButton">Usuário</Button>*/}
                    {/*</Dropdown>*/}
                </Col>
            </Row>

        </Header>
    );
}