import axios from "axios";
import { getToken } from "./Auth/auth";

const api = axios.create({
    baseURL: "localhost:7000"
});

api.interceptors.request.use(async config => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
});

export default api;