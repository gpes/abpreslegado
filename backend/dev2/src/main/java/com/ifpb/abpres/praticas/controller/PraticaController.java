package com.ifpb.abpres.praticas.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifpb.abpres.exception.ResourceNotFoundException;
import com.ifpb.abpres.praticas.model.Pratica;
import com.ifpb.abpres.praticas.service.PraticaService;

@RestController
@RequestMapping("/api/praticas")
public class PraticaController {

	@Autowired
	private PraticaService praticaService;
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(value = "/{categoriaId}")
	public ResponseEntity<Pratica> create(@Valid @RequestBody Pratica pratica , @PathVariable Long categoriaId) {
		Pratica created = praticaService.create(pratica, categoriaId);
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<Pratica>> getAll() {
		return ResponseEntity.ok(praticaService.getAll());
	}
	
	@CrossOrigin
	@GetMapping(value = "/{praticaId}")
	public ResponseEntity<Pratica> getOne(@PathVariable Long praticaId) {
		Optional<Pratica> pratica = praticaService.getOne(praticaId);
		
		if (pratica.isPresent())
			return ResponseEntity.ok(pratica.get());
		
		throw new ResourceNotFoundException(praticaId);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@PutMapping(value = "/{praticaId}")
	public ResponseEntity<Pratica> update(@PathVariable Long praticaId, @Valid @RequestBody Pratica pratica) {
		Pratica updated = praticaService.update(praticaId, pratica);
		return ResponseEntity.ok(updated);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@DeleteMapping(value = "/{praticaId}")
	public ResponseEntity<Pratica> delete(@PathVariable Long praticaId) {
		Pratica deleted = praticaService.delete(praticaId);
		return ResponseEntity.ok(deleted);
	}
}
