package com.ifpb.abpres.usuarios.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.exception.ResourceNotFoundException;
import com.ifpb.abpres.exception.UnauthorizedException;
import com.ifpb.abpres.usuarios.model.Usuario;
import com.ifpb.abpres.usuarios.repository.IUsuarioRepository;
import com.ifpb.abpres.usuarios.util.AccountCredentials;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class UsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	public Usuario autenticar(AccountCredentials ac) {
		Optional<Usuario> encontrado = usuarioRepository.findByUsernameOrEmail(ac.getLogin(), ac.getLogin());

		if (encontrado.isPresent()) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			Usuario u = encontrado.get();
			
			if (encoder.matches(ac.getPassword(), u.getSenha())) {
				return u;
			}
		}
			
		throw new UnauthorizedException();
	}

	public String generateToken(String username, String secret, Long expTime) {
		return Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + expTime))
				.signWith(SignatureAlgorithm.HS512, secret)
				.compact();
	}

	public Usuario create(Usuario usuario) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		usuario.setSenha(encoder.encode(usuario.getSenha()));
		return usuarioRepository.save(usuario);
	}

	public List<Usuario> getAll() {
		return usuarioRepository.findAll();
	}

	public Optional<Usuario> getOne(Long usuarioId) {
		return usuarioRepository.findById(usuarioId);
	}

	public Usuario update(Long usuarioId, Usuario usuario) {
		Optional<Usuario> usuarioOpt = usuarioRepository.findById(usuarioId);

		if (usuarioOpt.isPresent()) {
			usuario.setId(usuarioId);
			return usuarioRepository.save(usuario);
		}

		throw new ResourceNotFoundException(usuarioId);
	}

	public Usuario delete(Long usuarioId) {
		Optional<Usuario> usuarioOpt = usuarioRepository.findById(usuarioId);

		if (!usuarioOpt.isPresent())
			throw new ResourceNotFoundException(usuarioId);

		usuarioRepository.deleteById(usuarioId);
		return usuarioOpt.get();
	}
}
