package com.ifpb.abpres.praticas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ifpb.abpres.praticas.model.Pratica;

public interface PraticaRepository extends JpaRepository<Pratica, Long> {
	
}
