package com.ifpb.abpres.categorias.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ifpb.abpres.categorias.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
