package com.ifpb.abpres.classificacoes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ifpb.abpres.classificacoes.model.Classificacao;

public interface ClassificacaoRepository extends JpaRepository<Classificacao,Long> {

}
