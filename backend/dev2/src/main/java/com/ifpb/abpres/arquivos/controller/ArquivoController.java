package com.ifpb.abpres.arquivos.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.ifpb.abpres.praticas.model.Pratica;
import com.ifpb.abpres.praticas.service.PraticaService;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.ifpb.abpres.arquivos.model.Arquivo;
import com.ifpb.abpres.arquivos.service.ArquivoService;
import com.ifpb.abpres.exception.ResourceNotFoundException;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/arquivos")
public class ArquivoController {

    @Autowired
    private ArquivoService arquivoService;
// IMPLEMENTAR UM METODO PRA DOWNLOAD QUE ESSE METODO APENAS LISTA
    //	@Autowired
    private PraticaService praticaService;

//	@GetMapping(value = "/{arquivoId}/download")
//	public ResponseEntity<Arquivo> downloadArquivo(@PathVariable Long arquivoId) {
//		Arquivo arquivo = arquivoService.download(arquivoId);
//		return ResponseEntity.ok(arquivo);
//	}

    @GetMapping()
    public ResponseEntity<List<Arquivo>> getAll() {
        return ResponseEntity.ok(arquivoService.getAll());
    }

    @GetMapping(value = "/{arquivoId}")
    public ResponseEntity<Arquivo> getOne(@PathVariable Long arquivoId) {
        Optional<Arquivo> arquivo = arquivoService.getOne(arquivoId);

        if (arquivo.isPresent())
            return ResponseEntity.ok(arquivo.get());

        throw new ResourceNotFoundException(arquivoId);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PutMapping(value = "/{arquivoId}")
    public ResponseEntity<Arquivo> update(@PathVariable Long arquivoId, @Valid @RequestBody Arquivo arquivo) {
        Arquivo updated = arquivoService.update(arquivoId, arquivo);if(updated ==null)
			throw new ResourceNotFoundException(arquivoId);
        return ResponseEntity.ok(arquivo);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{arquivoId}")
    public ResponseEntity<Arquivo> delete(@PathVariable Long arquivoId) {
        Arquivo deleted = arquivoService.delete(arquivoId);
        return ResponseEntity.ok(deleted);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping("/adicionar")
    public ResponseEntity<Arquivo> add(@Valid @ModelAttribute Arquivo arquivo, @RequestParam Long praticaId,
                                       @RequestParam MultipartFile file) {
        Optional<Pratica> pratica = praticaService.getOne(praticaId);

        System.out.println(arquivo);

        if (pratica.isPresent()){
            String path = arquivoService.uploadFile(file);
            Pratica p = pratica.get();
            arquivo.setPratica(p);
            arquivo.setPath(path);
            p.getArquivos().add(arquivo);
            praticaService.update(p.getId(), p);
            Arquivo created = arquivoService.create(arquivo);
            return ResponseEntity.ok(created);
        }

        throw new ResourceNotFoundException(praticaId);
    }

}
