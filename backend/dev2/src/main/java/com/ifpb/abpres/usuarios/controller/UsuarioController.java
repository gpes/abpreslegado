package com.ifpb.abpres.usuarios.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifpb.abpres.exception.ResourceNotFoundException;
import com.ifpb.abpres.usuarios.model.Usuario;
import com.ifpb.abpres.usuarios.service.UsuarioService;
import com.ifpb.abpres.usuarios.util.AccountCredentials;

@RestController
@RequestMapping("/api/usuarios")
public class UsuarioController {
	static final String TOKEN_PREFIX = "Bearer";
	static final String SECRET = "AbpresSecret";
	static final String HEADER_STRING = "Authorization";
	
	@Autowired
	private UsuarioService usuarioService;
	
   
	@CrossOrigin
	@PostMapping(value = "/autenticar")
	public ResponseEntity<Usuario> autenticarUsuario(HttpServletRequest request,
			HttpServletResponse response,
			@Valid @RequestBody AccountCredentials ac) {
		
		Usuario encontrado = usuarioService.autenticar(ac);
		String token = usuarioService.generateToken(encontrado.getUsername(), SECRET, 7200000L);
		
		response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + token);
		response.setHeader("Access-Control-Expose-Headers",
			    "Authorization");
		return ResponseEntity.ok(encontrado);
	}
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@PostMapping()
	public ResponseEntity<Usuario> create(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody Usuario usuario) {
		Usuario created = usuarioService.create(usuario);
		created.setSenha("");

		
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<Usuario>> getAll() {
		return ResponseEntity.ok(usuarioService.getAll());
	}
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@GetMapping(value = "/{usuarioId}")
	public ResponseEntity<Usuario> getOne(@PathVariable Long usuarioId) {
		Optional<Usuario> usuario = usuarioService.getOne(usuarioId);
		
		if (usuario.isPresent())
			return ResponseEntity.ok(usuario.get());
		
		throw new ResourceNotFoundException(usuarioId);
	}
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@PutMapping(value = "/{usuarioId}")
	public ResponseEntity<Usuario> update(@PathVariable Long usuarioId, @Valid @RequestBody Usuario usuario) {
		Usuario updated = usuarioService.update(usuarioId, usuario);
		return ResponseEntity.ok(updated);
	}
	
    @PreAuthorize("hasAnyRole('ADMIN')")
	@CrossOrigin
	@DeleteMapping(value = "/{usuarioId}")
	public ResponseEntity<Usuario> delete(@PathVariable Long usuarioId) {
		Usuario deleted = usuarioService.delete(usuarioId);
		return ResponseEntity.ok(deleted);
	}
}
