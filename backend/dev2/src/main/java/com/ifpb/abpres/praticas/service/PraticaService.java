package com.ifpb.abpres.praticas.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.categorias.model.Categoria;
import com.ifpb.abpres.categorias.repository.CategoriaRepository;
import com.ifpb.abpres.exception.ResourceNotFoundException;
import com.ifpb.abpres.praticas.model.Pratica;
import com.ifpb.abpres.praticas.repository.PraticaRepository;

@Service
public class PraticaService {
	
	@Autowired
	private PraticaRepository praticaRepository;
	@Autowired
	private CategoriaRepository categoriaRepository;

	
	public Pratica create(Pratica pratica, Long categoriaId) {
		Optional<Categoria> cat = categoriaRepository.findById(categoriaId);
			pratica.getCategorias().add(cat.get());
			cat.get().getPraticas().add(pratica); 
			praticaRepository.save(pratica);
			categoriaRepository.save(cat.get());
			return pratica;
	}
	
	public List<Pratica> getAll() {
		return praticaRepository.findAll();
	}

	public Optional<Pratica> getOne(Long praticaId) {
		return praticaRepository.findById(praticaId);
	}

	public Pratica update(Long praticaId, Pratica pratica) {
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);

		if (praticaOpt.isPresent()) {
			pratica.setId(praticaId);
			return praticaRepository.save(pratica);
		}

		throw new ResourceNotFoundException(praticaId);
	}

	public Pratica delete(Long praticaId) {
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);


		if (!praticaOpt.isPresent())
			throw new ResourceNotFoundException(praticaId);
		
		for (Categoria c : praticaOpt.get().getCategorias()) {
			c.getPraticas().remove(praticaOpt.get());
			categoriaRepository.save(c);
		}
		praticaOpt.get().getCategorias().clear();
		praticaRepository.deleteById(praticaId);
		return praticaOpt.get();
	}
}
