package com.ifpb.abpres.classificacoes.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.classificacoes.model.Classificacao;
import com.ifpb.abpres.classificacoes.repository.ClassificacaoRepository;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@Service
public class ClassificacaoService {
	
	@Autowired
	private ClassificacaoRepository classificacaoRepository;
	
	public List<Classificacao> getAll(){
		return classificacaoRepository.findAll();
	}
	
	public Optional<Classificacao> getOne(Long classificacaoId) {
		return classificacaoRepository.findById(classificacaoId);
	}

	public Classificacao create (Classificacao classificacao) {
		return classificacaoRepository.save(classificacao);
	}
	
	public Classificacao delete (Long classificacaoId) {
		Optional<Classificacao> classificacaoOpt = classificacaoRepository.findById(classificacaoId);
		
		if (!classificacaoOpt.isPresent()) {
			throw new ResourceNotFoundException(classificacaoId);
		}
		
		classificacaoRepository.deleteById(classificacaoId);
		return classificacaoOpt.get();
	}
	
	public Classificacao update (Long classificacaoId, Classificacao classificacao) {
		Optional<Classificacao> classificacaoOpt = classificacaoRepository.findById(classificacaoId);
		
		if (classificacaoOpt.isPresent()) {
			classificacao.setId(classificacaoId);
			return classificacaoRepository.save(classificacao);
		}
		throw new ResourceNotFoundException(classificacaoId);
	}
	
}
