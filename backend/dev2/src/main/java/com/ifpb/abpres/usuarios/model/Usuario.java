package com.ifpb.abpres.usuarios.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_USUARIO", uniqueConstraints=
@UniqueConstraint(columnNames={"email", "username"}))
@NoArgsConstructor
public class Usuario implements Serializable, UserDetails {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    @Getter @Setter
    private Long id;

    @NotNull
    @Size(max = 70)
    @Getter @Setter
    private String username;

    @NotNull
    @Email
    @Size(max = 100)
    @Getter @Setter
    private String email;

    @NotNull
    @Size(max = 128)
    @Getter @Setter
    private String senha;

    @NotNull
    @Getter @Setter
    private String tipo;

	public Usuario(String username, String email, String senha, Tipo tipo) {
		this.username = username;
		this.email = email;
		this.senha = senha;
		this.tipo = tipo.name();
	}
	
//	public Usuario(String username, String email, String senha, String tipo) {
//		this.username = username;
//		this.email = email;
//		this.senha = senha;
//		this.tipo = tipo;
//	}

	public Usuario() {

	}
	
	public Long getId() {
		return id;
	}

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		final String prefixo = "ROLE_";
		 List<GrantedAuthority> listaAutorizacao = new ArrayList<GrantedAuthority>();

		 listaAutorizacao.add(new SimpleGrantedAuthority(/*prefixo + */this.getTipo()));
		 
		 return listaAutorizacao;
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}

}
