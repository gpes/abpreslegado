package com.ifpb.abpres.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {		
		httpSecurity.csrf().disable().authorizeRequests()
		//.antMatchers(HttpMethod.GET, 		"/api/usuarios").permitAll()
		.antMatchers(HttpMethod.POST, 		"/api/usuarios/autenticar").permitAll()
		.antMatchers(HttpMethod.POST, 		"/api/usuarios").permitAll()

		
		.antMatchers(HttpMethod.GET, 		"/api/arquivos").permitAll()

		
		.antMatchers(HttpMethod.GET, 		"/api/categorias").permitAll()
		.antMatchers(HttpMethod.POST, 		"/api/categorias").permitAll()

		.antMatchers(HttpMethod.GET, 		"/api/classificacoes").permitAll()


		.antMatchers(HttpMethod.GET, 		"/api/praticas").permitAll()

		
		.anyRequest().authenticated()
		.and()

		.addFilterBefore(new JWTAuthenticationFilter(),
				UsernamePasswordAuthenticationFilter.class);
	}
 
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS);
	}

}
