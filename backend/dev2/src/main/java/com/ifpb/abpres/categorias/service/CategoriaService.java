package com.ifpb.abpres.categorias.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.categorias.model.Categoria;
import com.ifpb.abpres.categorias.repository.CategoriaRepository;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository categoriaRepository;
	
	public Categoria create(Categoria categoria) {
		return categoriaRepository.save(categoria);
	}
	
	public List<Categoria> getAll() {
		return categoriaRepository.findAll();
	}

	public Optional<Categoria> getOne(Long categoriaId) {
		return categoriaRepository.findById(categoriaId);
	}

	public Categoria update(Long categoriaId, Categoria categoria) {
		Optional<Categoria> categoriaOpt = categoriaRepository.findById(categoriaId);

		if (categoriaOpt.isPresent()) {
			categoria.setId(categoriaId);
			return categoriaRepository.save(categoria);
		}

		throw new ResourceNotFoundException(categoriaId);
	}

	public Categoria delete(Long categoriaId) {
		Optional<Categoria> categoriaOpt = categoriaRepository.findById(categoriaId);

		if (!categoriaOpt.isPresent())
			throw new ResourceNotFoundException(categoriaId);

		categoriaRepository.deleteById(categoriaId);
		return categoriaOpt.get();
	}
}
