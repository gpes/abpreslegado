package com.ifpb.abpres;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ifpb.abpres.usuarios.model.Tipo;
import com.ifpb.abpres.usuarios.model.Usuario;
import com.ifpb.abpres.usuarios.repository.IUsuarioRepository;

@SpringBootApplication
public class AbpresApplication implements CommandLineRunner {

	@Autowired
	private Environment env;

	@Autowired
	private IUsuarioRepository usuarioRepository;
	
	

	public static void main(String[] args) {
		SpringApplication.run(AbpresApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		this.usuarioSeeder();
	}
	
	public void usuarioSeeder() {
		Optional<Usuario> u = usuarioRepository.findByUsernameOrEmail(
				env.getProperty("setup.admin.username"), env.getProperty("setup.admin.email"));

		if (!u.isPresent()) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

			Usuario toSave = new Usuario(env.getProperty("setup.admin.username"),
					env.getProperty("setup.admin.email"),
					encoder.encode(env.getProperty("setup.admin.password")),
					Tipo.ADMIN);
//			
//			Usuario toSave = new Usuario(env.getProperty("setup.admin.username"),
//					env.getProperty("setup.admin.email"),
//					encoder.encode(env.getProperty("setup.admin.password")),
//					env.getProperty("setup.admin.papel"));
			
			usuarioRepository.save(toSave);
		}
	}
	
}
