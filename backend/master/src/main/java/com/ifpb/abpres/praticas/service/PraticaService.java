package com.ifpb.abpres.praticas.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.exception.ResourceNotFoundException;
import com.ifpb.abpres.praticas.model.Pratica;
import com.ifpb.abpres.praticas.repository.PraticaRepository;

@Service
public class PraticaService {
	
	@Autowired
	private PraticaRepository praticaRepository;
	
	public Pratica create(Pratica pratica) {
		return praticaRepository.save(pratica);
	}
	
	public List<Pratica> getAll() {
		return praticaRepository.findAll();
	}

	public Optional<Pratica> getOne(Long praticaId) {
		return praticaRepository.findById(praticaId);
	}

	public Pratica update(Long praticaId, Pratica pratica) {
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);

		if (praticaOpt.isPresent()) {
			pratica.setId(praticaId);
			return praticaRepository.save(pratica);
		}

		throw new ResourceNotFoundException(praticaId);
	}

	public Pratica delete(Long praticaId) {
		Optional<Pratica> praticaOpt = praticaRepository.findById(praticaId);

		if (!praticaOpt.isPresent())
			throw new ResourceNotFoundException(praticaId);

		praticaRepository.deleteById(praticaId);
		return praticaOpt.get();
	}
}
