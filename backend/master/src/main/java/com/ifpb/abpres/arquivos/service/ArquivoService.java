package com.ifpb.abpres.arquivos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.arquivos.model.Arquivo;
import com.ifpb.abpres.arquivos.repository.ArquivoRepository;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@Service
public class ArquivoService {

	@Autowired
	private ArquivoRepository arquivoRepository;

	public Arquivo download(Long arquivoId) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (arquivoOpt.isPresent()) {
			return arquivoOpt.get();
		}

		throw new ResourceNotFoundException(arquivoId);
	}
	
	public List<Arquivo> getAll() {
		return arquivoRepository.findAll();
	}

	public Optional<Arquivo> getOne(Long arquivoId) {
		return arquivoRepository.findById(arquivoId);
	}

	public Arquivo update(Long arquivoId, Arquivo arquivo) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (arquivoOpt.isPresent()) {
			arquivo.setId(arquivoId);
			return arquivoRepository.save(arquivo);
		}

		throw new ResourceNotFoundException(arquivoId);
	}

	public Arquivo delete(Long arquivoId) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (!arquivoOpt.isPresent())
			throw new ResourceNotFoundException(arquivoId);

		arquivoRepository.deleteById(arquivoId);
		return arquivoOpt.get();
	}
}
