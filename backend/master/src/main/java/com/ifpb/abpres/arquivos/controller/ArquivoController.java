package com.ifpb.abpres.arquivos.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifpb.abpres.arquivos.model.Arquivo;
import com.ifpb.abpres.arquivos.service.ArquivoService;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/arquivos")
public class ArquivoController {

	@Autowired
	private ArquivoService arquivoService;
	
//	@GetMapping(value = "/{arquivoId}")
//	public ResponseEntity<Arquivo> downloadArquivo(@PathVariable Long arquivoId) {
//		Arquivo arquivo = arquivoService.download(arquivoId);
//		return ResponseEntity.ok(arquivo);
//	}
	
	@GetMapping()
	public ResponseEntity<List<Arquivo>> getAll() {
		return ResponseEntity.ok(arquivoService.getAll());
	}
		
	@GetMapping(value = "/{arquivoId}")
	public ResponseEntity<Arquivo> getOne(@PathVariable Long arquivoId) {
		Optional<Arquivo> arquivo = arquivoService.getOne(arquivoId);
		
		if (arquivo.isPresent())
			return ResponseEntity.ok(arquivo.get());
		
		throw new ResourceNotFoundException(arquivoId);
	}
	
//	@PutMapping(value = "/{arquivoId}")
//	public ResponseEntity<Arquivo> update(@PathVariable Long arquivoId, @Valid @RequestBody Arquivo arquivo) {
//		Arquivo updated = arquivoService.update(arquivoId, arquivo);
//		return ResponseEntity.ok(arquivo);
//	}
	
	@DeleteMapping(value = "/{arquivoId}")
	public ResponseEntity<Arquivo> delete(@PathVariable Long arquivoId) {
		Arquivo deleted = arquivoService.delete(arquivoId);
		return ResponseEntity.ok(deleted);
	}
	
}
