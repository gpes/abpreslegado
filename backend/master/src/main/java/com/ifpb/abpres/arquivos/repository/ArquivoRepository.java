package com.ifpb.abpres.arquivos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ifpb.abpres.arquivos.model.Arquivo;

public interface ArquivoRepository extends JpaRepository<Arquivo, Long> {

}
