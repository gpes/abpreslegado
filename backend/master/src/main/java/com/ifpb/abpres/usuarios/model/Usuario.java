package com.ifpb.abpres.usuarios.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "TB_USUARIO", uniqueConstraints=
@UniqueConstraint(columnNames={"email", "username"}))
@NoArgsConstructor
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	@Getter @Setter
    private Long id;
	
	@NotNull
    @Size(max = 70)
	@Getter @Setter
	private String username;

	@NotNull
    @Email
    @Size(max = 100)
	@Getter @Setter
	private String email;
	
	@NotNull
    @Size(max = 128)
	@Getter @Setter
	private String senha;
	
	@NotNull
	@Getter @Setter
	private String tipo;

	public Usuario(String username, String email, String senha, Tipo tipo) {
		this.username = username;
		this.email = email;
		this.senha = senha;
		this.tipo = tipo.name();
	}
	
	public Usuario() {
		
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
}
