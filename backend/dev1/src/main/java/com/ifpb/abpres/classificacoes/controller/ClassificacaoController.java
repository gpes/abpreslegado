package com.ifpb.abpres.classificacoes.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifpb.abpres.classificacoes.model.Classificacao;
import com.ifpb.abpres.classificacoes.service.ClassificacaoService;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/classificacoes")
public class ClassificacaoController {
	
	@Autowired
	private ClassificacaoService classificacaoService;
	
	@GetMapping
	public ResponseEntity<List<Classificacao>> getAll(){
		return ResponseEntity.ok(classificacaoService.getAll());
	}
	
	@GetMapping(value = "/{classificacaoId}")
	public ResponseEntity<Classificacao> getOne(@PathVariable Long classificacaoId){
		Optional<Classificacao> classificacaoOpt = classificacaoService.getOne(classificacaoId);
		
		if(classificacaoOpt.isPresent()) {
			return ResponseEntity.ok(classificacaoOpt.get());
		}
		
		throw new ResourceNotFoundException(classificacaoId);
	}
	
	@PutMapping(value = "/{classificacaoId}")
	public ResponseEntity<Classificacao> update(@PathVariable Long classificacaoId, @Valid @RequestBody Classificacao classificacao){
		Classificacao atualizado = classificacaoService.update(classificacaoId, classificacao);
		
		if (atualizado == null) {
				throw new ResourceNotFoundException(classificacaoId);
		}
		return ResponseEntity.ok(classificacao);
	}
	
	@DeleteMapping(value = "/{classificacaoId}")
	public ResponseEntity<Classificacao> delete(@PathVariable Long classificacaoId){
		Classificacao removido = classificacaoService.delete(classificacaoId);
		
		if (removido == null) {
			throw new ResourceNotFoundException(classificacaoId);
		}
		return ResponseEntity.ok(removido);
	}

}
