package com.ifpb.abpres.categorias.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ifpb.abpres.categorias.model.Categoria;
import com.ifpb.abpres.categorias.service.CategoriaService;
import com.ifpb.abpres.exception.ResourceNotFoundException;

@RestController
@RequestMapping("/api/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaService categoriaService;
	
	@CrossOrigin
	@PostMapping()
	public ResponseEntity<Categoria> create(@Valid @RequestBody Categoria categoria) {
		Categoria created = categoriaService.create(categoria);
		return ResponseEntity.status(HttpStatus.CREATED).body(created); 
	}
	
	@CrossOrigin
	@GetMapping()
	public ResponseEntity<List<Categoria>> getAll() {
		return ResponseEntity.ok(categoriaService.getAll());
	}
	
	@CrossOrigin
	@GetMapping(value = "/{categoriaId}")
	public ResponseEntity<Categoria> getOne(@PathVariable Long categoriaId) {
		Optional<Categoria> categoria = categoriaService.getOne(categoriaId);
		
		if (categoria.isPresent())
			return ResponseEntity.ok(categoria.get());
		
		throw new ResourceNotFoundException(categoriaId);
	}
	
	@CrossOrigin
	@PutMapping(value = "/{categoriaId}")
	public ResponseEntity<Categoria> update(@PathVariable Long categoriaId, @Valid @RequestBody Categoria categoria) {
		Categoria updated = categoriaService.update(categoriaId, categoria);
		return ResponseEntity.ok(updated);
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/{categoriaId}")
	public ResponseEntity<Categoria> delete(@PathVariable Long categoriaId) {
		Categoria deleted = categoriaService.delete(categoriaId);
		return ResponseEntity.ok(deleted);
	}
}
