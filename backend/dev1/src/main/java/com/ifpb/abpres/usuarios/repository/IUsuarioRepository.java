package com.ifpb.abpres.usuarios.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ifpb.abpres.usuarios.model.Usuario;

public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {

	public Optional<Usuario> findByUsernameOrEmail(String username, String email);

}
