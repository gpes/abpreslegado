package com.ifpb.abpres.categorias.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ifpb.abpres.praticas.model.Pratica;

import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_CATEGORIA")
@NoArgsConstructor
public class Categoria implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_categoria")
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String titulo;	
	
	@JsonIdentityInfo(
			  generator = ObjectIdGenerators.PropertyGenerator.class, 
			  property = "id")
	//Parte dominante do many to many
    @ManyToMany
    //Nome da nova tabela que será criada para implementar o many to many
    @JoinTable(name="praticas_categorias", joinColumns=
    //Join entre pratica e categoria
    {@JoinColumn(name="pratica_id")}, inverseJoinColumns=
      {@JoinColumn(name="categoria_id")})
    private List<Pratica> praticas = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Pratica> getPraticas() {
		return praticas;
	}

	public void setPraticas(List<Pratica> praticas) {
		this.praticas = praticas;
	}

}
