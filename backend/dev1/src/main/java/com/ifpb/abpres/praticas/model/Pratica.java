package com.ifpb.abpres.praticas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.ifpb.abpres.arquivos.model.Arquivo;
import com.ifpb.abpres.categorias.model.Categoria;

import lombok.NoArgsConstructor;

@Entity
@Table(name = "TB_PRATICA")
@NoArgsConstructor
public class Pratica implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pratica")
	private Long id;

	@NotBlank
	@Size(max = 100)
	private String titulo;


	@OneToMany(mappedBy = "pratica", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Arquivo> arquivos = new ArrayList<>();

	//Impede que ocorra uma recursividade inifinita na hora de printar o objeto (USADO PARA MANY TO MANY)
	@JsonIdentityInfo(
			  generator = ObjectIdGenerators.PropertyGenerator.class,
			  property = "id")

	//Jsonigore usado caso queira omitir a exibição do arraylist de categorias em uma pratica
	//@JsonIgnore
	//Parte não dominante do many to many
    @ManyToMany(mappedBy="praticas")
    private List<Categoria> categorias = new ArrayList<>();


	public Boolean addCat(Categoria c) {
		categorias.add(c);
		return true;
	}

	public List<Categoria> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<Arquivo> getArquivos() {
		return arquivos;
	}

	public void setArquivos(List<Arquivo> arquivos) {
		this.arquivos = arquivos;
	}

}
