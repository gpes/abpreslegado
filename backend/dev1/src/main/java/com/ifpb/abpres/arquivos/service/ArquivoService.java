package com.ifpb.abpres.arquivos.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Optional;

import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.ifpb.abpres.arquivos.model.Arquivo;
import com.ifpb.abpres.arquivos.repository.ArquivoRepository;
import com.ifpb.abpres.exception.ResourceNotFoundException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;

@Service
public class ArquivoService {

	@Autowired
	private ArquivoRepository arquivoRepository;

	@Autowired
	ServletContext context;

	public Arquivo download(Long arquivoId) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (arquivoOpt.isPresent()) {
			return arquivoOpt.get();
		}

		throw new ResourceNotFoundException(arquivoId);
	}
	
	public List<Arquivo> getAll() {
		return arquivoRepository.findAll();
	}

	public Optional<Arquivo> getOne(Long arquivoId) {
		return arquivoRepository.findById(arquivoId);
	}

	public Arquivo update(Long arquivoId, Arquivo arquivo) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (arquivoOpt.isPresent()) {
			arquivo.setId(arquivoId);
			return arquivoRepository.save(arquivo);
		}

		throw new ResourceNotFoundException(arquivoId);
	}

	public Arquivo delete(Long arquivoId) {
		Optional<Arquivo> arquivoOpt = arquivoRepository.findById(arquivoId);

		if (!arquivoOpt.isPresent())
			throw new ResourceNotFoundException(arquivoId);

		arquivoRepository.deleteById(arquivoId);
		return arquivoOpt.get();
	}

	public Arquivo create(Arquivo arquivo) {
		return arquivoRepository.save(arquivo);
	}

	public String uploadFile(MultipartFile file) {

		if (file.isEmpty()) {
			throw new StorageException("Failed to store empty file");
		}

		try {
			byte[] bytes = file.getBytes();
			Path path = Paths.get(context.getRealPath("uploads") + file.getOriginalFilename());
			Files.write(path, bytes);
			return Paths.get(context.getRealPath("uploads") + file.getOriginalFilename()).toString();
		} catch (IOException e) {

			var msg = String.format("Failed to store file", file.getName());

			throw new StorageException(msg, e);
		}

	}
}
