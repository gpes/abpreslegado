package com.ifpb.abpres.arquivos.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.ifpb.abpres.classificacoes.model.Classificacao;
import com.ifpb.abpres.praticas.model.Pratica;

import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "TB_ARQUIVO")
@NoArgsConstructor
@ToString
public class Arquivo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_arquivo")
    private Long id;
	
	@NotBlank
    @Size(max = 100)
	private String nome;

    @Size(max = 250)
	private String path;
	
	@NotBlank
	@Size(max = 250)
	private String descricao;

	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JoinColumn(name = "id_pratica")
	private Pratica pratica;

	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL}, optional = true)
	@JoinColumn(name = "classificacao_id", nullable = true)
	private Classificacao classificacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Pratica getPratica() {
		return pratica;
	}

	public void setPratica(Pratica pratica) {
		this.pratica = pratica;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
	
}
